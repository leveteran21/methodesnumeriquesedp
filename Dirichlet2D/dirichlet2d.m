function [err, soldir]=dirichlet2d(a,b,c)

  disp('Choisir un pas de maillage, par exemple h = 0.25 ou 0.1 ou 0.05 ou 0.02 ')
  h = input('Effectuez votre choix, par exemple  h = 0.25 ou 0.1 ou 0.05 ou 0.02 '); 
  disp('Vous avez choisi h = ')
  disp(h)

  % Maillage du domaine 
  % ------------------------------------------------------------
  
  % On peut egalement opter pour une autre consideration de h en faisant   
  % usage de la distance euclidienne en posant : h  = sqrt(2)*k, 
  % avec k = (b-a)/N sur l'axe (Ox).
  % En realite cette consideration vient du fait que dans le cas general
  % on a h = max(diam(T), T element geometrique), ici en l'occurrence
  %  T est un carre de cote k. Il vient donc dans ce cas :
  
  %k = h/sqrt(2) ;
  
  %N = round((b-a)/k); 
  %M = round((c-a)/k);
 
  %k_1 = (b-a)/N;
  %k_2 = (c-a)/M;
  %disp(M)
  %disp(N)
  %h_1 = sqrt(2)*k_1 ;
  %h_2 = sqrt(2)*k_2 ;
  
  % Determination du nombre de points de discretisation du domaine
  % Ces calculs de h sont pris dans le cas de la distance en norme Inf
  % dans ce cas le pas h = k ou k = (b-a)/N sur l'axe (Ox) par exemple.
  
  N = round((b-a)/h);
  M = round((c-a)/h);

  h_1 = (b-a)/N;
  h_2 = (c-a)/M;

  % Pas de maillage considere 

  h_evaluated = max(h_1,h_2) ;
  h = h_evaluated;

  disp('Pas de maillage considere : h = ')
  disp(h)
  % Nombre de grilles 
  Nb_grilles = N*M ;
  
  disp('Nombre de grilles : Nb_grilles = ')
  disp(Nb_grilles)
  % -----------------------------------------------------------------
  % nombre de sommets internes (N-1)x(M-1)
    nbint = (N-1)*(M-1);
    
    disp('nombre de noeuds internes = ')
    disp(nbint)
    % nombre de sommets sur le bord 2*(N+1) + 2*(M-1)
    nbsbord = 2*(N+1) + 2*(M-1);
    
    disp('nombre de noeuds externes = ')
    disp(nbsbord)
    % nombre de sommets du maillage
    nbsom = nbint + nbsbord;
    
    disp('nombre de sommets du maillage = ')
    disp(nbsom)
  %--------------------------------------------------------------------
  % Les points de discretisation sur le bord du domaine
  % xy(i,j), x = i*h, 0 <=i<=N et y = 0
  xyi_0(1:2,1:(N+1)) = 0.0;
  xyi_0(1:2,1) = 0.0 ;
  
  for i = 2:(N+1)
    xyi_0(1,i) = xyi_0(1,i-1) + h ;
    xyi_0(2,i) = 0.0 ;
  end
  %xyi_0
  %--------------------------------------------------------------------
  % xy(i,j), x = i*h, 0 <=i<=N et y = 1
  xyi_1(1:2,1:(N+1)) = 0.0;
  xyi_1(1:2,1) = [0.0,1.0] ;
  
  for i = 2:(N+1)
    xyi_1(1,i) = xyi_1(1,i-1) + h ;
    xyi_1(2,i) = 1.0 ;
  end
  %xyi_1
  %--------------------------------------------------------------------
  % xy(i,j), y = j*h, 0 <=j<=N et x = 0
  xy0_j(1:2,1:M-1) = 0.0;
  xy0_j(1:2,1) = [0,0.25] ;
  for j = 2:M-1
    xy0_j(1,j) = 0.0 ;
    xy0_j(2,j) = xy0_j(2,j-1) + h ;
  end
  %xy0_j
  %--------------------------------------------------------------------
  % xy(i,j), y = j*h, 0 <=j<=N et x = 1
  xy1_j(1:2,1:M-1) = 0.0;
  xy1_j(1:2,1) = [1.0,0.25] ;
  for j = 2:M-1
    xy1_j(1,j) = 1.0 ;
    xy1_j(2,j) = xy1_j(2,j-1) + h ;
  end
  %xy1_j
  %--------------------------------------------------------------------- 
  % Determination des noeuds internes 
  % xydiscrete(i,j), 1 <= i,j <= nbint
  xydiscrete(1:2, 1:nbint) = 0.0 ;
  m = 0;
  for i = 1:(N-1) 
    for j = 1:(M-1)
        m = m + 1;
        xydiscrete(1,m) = i*h; %1er coordonee du sommet m
        xydiscrete(2,m) = j*h; %2eme coordonee du sommet m
    end
  end 
  %disp(xydiscrete)
  % Determination des coefficients relatifs a chaque noeud interne
  % --------------------------------------------------------------

  % Coefficients provenant de f
  % fvalues_i_j : valeur de f au ieme noeud 
  fvalues(1:nbint)= 0.0;
  for i = 1:nbint 
    fvalues(i) = feval('f', xydiscrete(1,i), xydiscrete(2,i)) ;
  end
  % c'est le second membre
  b = (h^2)*fvalues;
  
  % ---------------------------------------------------------------
  % Saisie de la matrice A
  N = N-1; % on va de 1 à N-1 pour exclure les points sur le bord sur (Ox)
  M = M-1; % on va de 1 à M-1 pour exclure les points sur le bord sur (Oy)
  %A(1:N^2, 1:M^2) = 0.0;
  % la matrice block D
  %D(1:N,1:M) = 0.0;
  %for i = 1:N
  %    D(i,i) = -1;
  %end
  %disp(D)
  % la matrice block C
  C(1:N, 1:M) = 0.0;
  for i = 1:N
      C(i,i) = 4;
  end
  for i = 1:N-1
      C(i,i+1) = -1;
  end
  for i = 1:N-1
      C(i+1,i) = -1;
  end
  % matrice A de taille N^2 x M^2
  A = repmat({C},1,N); % on repète la matrice C N-fois
  A = blkdiag(A{:}); % crée la matrice de diagonales C
  % matrice triangulaire supérieure formé de D
  Aup = sparse(1:(N^2-N),(N+1):N^2,-ones(1,N^2-N),N^2,N^2,N^2-N);
  A = Aup'+ A + Aup;
  %disp(A)
  
  % Repartition des coefficients non nuls de la matrice du systeme

  disp('Visualisez la repartition des coefficients non nuls de la matrice')
  disp('Et saisir << return >> pour la suite des calculs')
  spy(A)

  keyboard
  % Calcul de la solution de A X = B :
  % ---------------------------------

  % X : approximation par Differences Finies de (u(xdiscrete(i)))_{1 <= i <= nbint} 
  X = A\b' ;
  %disp(X)
  % Consideration de la solution discrete sur tout le domaine 
  ugam(1:nbsbord)= 0.0;
  soldir = [X ; ugam'] ; % Ici je me permets car u = 0 sur le bord
  %disp(soldir)
  %disp(size(soldir))% la taille de soldir est bien égale à nbsom 
  
  % Consideration de la solution exacte dans la base discrete 
  for i = 1:nbint
      u(i) = feval('uex', xydiscrete(1,i),xydiscrete(2,i)) ;
  end
  u = [u , ugam];
  %disp(u)
  %disp(size(u))
  erreur = norm(u' - soldir, inf)/norm(u', inf) ;
  err    = 100 * erreur ;

  disp('Erreur commise en % avec la methode de discretisation est de :')
  disp(err)
  
  % Representation graphique de la solution
  
  % Tous les points (x_i, y_j) 0 <= i,j <= nbsom du domaine
  X(1:2,1:nbsom) = 0.0;
  X = [xydiscrete, xyi_0, xy1_j, xyi_1, xy0_j]; 
  XX(1:nbsom) = 0.0;
  YY(1:nbsom) =0.0;
  XX = X(1,1:nbsom); % ensemble des abscisses du domaine
  YY = X(2,1:nbsom); % ensemble des ordonées du domaine
  % Les valeurs de u en tous les points (x_i, y_j) 0 <= i,j <= nbsom 
  U(1:nbsom) = soldir; % les solutions du P.A.L.
 
  ti = 0:0.1:1; % table intermediaire
  [XI, YI] = meshgrid(ti,ti);
  ZI = griddata(XX, YY, U, XI, YI);
  surf(XI, YI, ZI);
  colorbar
end